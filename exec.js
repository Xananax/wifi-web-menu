const { exec:_exec } = require('child_process');

const exec = (command) => new Promise( (ok, no) => {
  _exec(command, (err, stdout, stderr) => {
    if (err) {
      return no(err);
    }
    if(stderr){
      return no(stderr)
    }
    ok(stdout)
  });
})

module.exports = { exec }