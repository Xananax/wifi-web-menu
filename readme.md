# Wifi Web Menu

An experiment in connecting to wifi through the browser. Works in Linux only.

## Dependencies:

- `nmcli`
- `node`

If you're missing the `nmcli` dep, the script will warn you and provide instructions about how to install on Arch.

## Run:

```sh
node index.js
```

Then open `http://localhost:3000`