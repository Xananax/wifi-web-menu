const http = require('http')
const html = require('fs').readFileSync('./index.html',{ encoding:'utf8' })

const port = process.env.PORT || 3000
const {
  set_wifi_off,
  set_wifi_on,
  get_networks_list,
  get_connection_status,
  check_dependencies,
  connect_and_get_status
} = require('./get_wifi')

const url_map = {
  'on':set_wifi_on,
  'off':set_wifi_off,
  'list':get_networks_list,
  'status':get_connection_status,
  'dependencies': check_dependencies
}

const json_answer = async (res, fn) => {
  try{
    const data = await fn()
    res.writeHead(200, { 'Content-Type': 'application/json' });
    res.end(JSON.stringify({ success:true, data }))
  }catch(e){
    res.writeHead(500, { 'Content-Type': 'application/json' });
    res.end(JSON.stringify({ success:false, error:e.message }))
  }
}

http.createServer( async (req, res ) => {
  const [ url, ...args ] = req.url.replace(/^\/|\/$/g,'').replace(/\/+/g,'/').split('/')
  console.log('/'+url,args)
  if(url in url_map){
    await json_answer(res, url_map[url])
    return;
  }
  if(url === 'connect'){
    const [ ssid, pass ] = args
    await json_answer(res, connect_and_get_status.bind(null,ssid, pass))
    return;
  }
  if(url === "" || url === "index" || url === "home" || url === "index.html"){
    res.writeHead(200, { 'Content-Type': 'text/html' });
    res.end(html)
    return;
  }
  res.writeHead(404, { 'Content-Type': 'text/plain' });
  res.end('page not found')
}).listen(port, () => console.log(`server listening on http://localhost:${port}`))