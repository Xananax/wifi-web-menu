const { exec } = require('./exec');
const { verify_dependency_list } = require('./verify_dependency_list')

const check_dependencies = () => 
  verify_dependency_list([
    ['nmcli','networkmanager']
  ])


/**
 * turns wifi on
 */
const set_wifi_on = async () => exec(`nmcli radio wifi on`)

/**
 * turns wifi off
 */
const set_wifi_off = async () => exec(`nmcli radio wifi off`)


/**
 * get network list in an array of objects of shape:
 * ```js
 * { active:boolean, ssid:string, security:string[], signal }
 * ```
 * where:
 * - `active` is a boolean that says if you are connected
 * - `ssid` is the name of the network
 * - `security` an array of schemas (such as `"WPA1"` and `"WPA2"`)
 * - `signal` is an int from 0 to 100 determining signal strength
 */
const get_networks_list = async () => {
  const nmcli_return = await exec("nmcli --terse --mode tabular --fields ACTIVE,SSID,SECURITY,SIGNAL device wifi list")
  const networks = nmcli_return.split('\n').filter(Boolean)
  return networks.map( n=> {
    const [ active, ssid, security, signal ] = n.split(':')
    return { active:active==='yes', ssid, security:security.split(' '), signal }
  })
}

/**
 * Connects to a specific network
 * @param {string} ssid 
 * @param {string} pass 
 */
const connect_to_network = async ( ssid, pass ) => {
  await exec(`nmcli device wifi connect ${ssid} password ${pass}`)
}

/**
 * Depending on connection status, 
 * returns an object of shape:
 * ```js
 * { connected: true, device: string, name: string }
 * ```
 * or
 * ```js
 * { connected: false }
 * ```
 * 
 * (note: `name` is the name nmcli gives the connection, not the ssid)
 */
const get_connection_status = async () => {
  try{
    const nmcli_return = await(exec(`nmcli -fields WIFI`))
    const [ device, name ] = nmcli_return.split('\n')[0].split(': connected to ')
    return { connected: true, device, name }
  }catch(e){
    return { connected: false }
  }
}

const connect_and_get_status = async (ssid, password) => {
  await connect_to_network(ssid, password)
  return await get_networks_list()
}

module.exports = {
  set_wifi_off,
  set_wifi_on,
  get_networks_list,
  get_connection_status,
  connect_to_network,
  connect_and_get_status,
  check_dependencies
}