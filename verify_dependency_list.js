const fs = require('fs');
const path = require('path');

const envPath = (process.env.PATH || '').replace(/["]+/g, '').split(path.delimiter)
const envExt = (process.env.PATHEXT || '').split(path.delimiter)

/**
 * Computes an array of all possible paths for an available binary in the system
 * @param {string} binaryName 
 */
const get_paths = ( binaryName ) => 
  envPath.reduce( 
    ( arr, chunk ) => [ ...arr, ...envExt.map( 
	  ext => path.join(chunk, binaryName + ext)
	)]
  ,[])

/**
 * Verifies a path exists and is accessible in the file system
 * @param {string} file_path 
 */
const file_exists = (file_path) => new Promise( ok => 
  fs.stat(file_path, (error, stat) => 
	ok( error ? false : stat.isFile())
  )
)

/**
 * Verifies a binary exists and is accessible
 * @param {string} binary_name a binary to check against all paths in the env var PATH
 */
const verify_dependency = async (binary_name) => {
  const paths_list = get_paths(binary_name)
  for(let path of paths_list){
	const found = await file_exists(path)
	if(found){ return found }
  }
  return false
} 

/**
 * Verifies passed dependencies are available in the system
 * Pass it an array of dependencies of the form:
 * ```js
 * verify_dependencies([{binary:'npm', package:'node'},{binary:'ssh', package:'openssh'},{binary:'htop', package:'htop'}]) 
 * // or
 * verify_dependencies([['npm','node'],['ssh','openssh'],['htop','htop']])
 * // or (the below forms don't provide missing packages hints )
 * verify_dependencies([{binary:'npm'},{binary:'ssh'},{binary:'htop'}]) 
 * verify_dependencies([['npm'],['ssh'],['htop']])
 * verify_dependencies(['npm','ssh','htop'])
 * // or mix it up!
 * verify_dependencies([{binary:'npm',package:'node'},['ssh','openssh'],'htop'])
 * ```
 * @param {array} dependencies_list an array of dependencies
 * @param {string} packageManager a string that will be used to hint at how to install missing packages. Defaults to "pacman -S %s --needed" (the "%s" will be replaced by packages names)
 * @returns true if all dependencies are satisfied
 * @throws if all dependencies are not satisfied
 */
const verify_dependency_list = async (dependencies_list, packageManager="pacman -S %s --needed") => {
  if(!Array.isArray(dependencies_list)){
    throw new Error('dependencies list is empty')
  }
  const missing_binaries = []
  const missing_packages = []
  const conform_dependencies_list = dependencies_list.map( dep => 
	typeof dep === 'string' 
	? {binary:dep} 
	: Array.isArray(dep) 
		? { binary: dep[0], package: dep[1] } 
		: dep 
  )
  for(let dependency of conform_dependencies_list){
	const { binary } = dependency
	const found = await verify_dependency(binary)
	if(!found){
	  missing_binaries.push(dependency.binary)
	  if( missing_packages.indexOf(dependency.package) < 0 ){
		missing_packages.push(dependency.package)
	  }
	}
  }
  if(!missing_packages.length){ 
	return true
  }
  const missing_binaries_str = missing_binaries.reduce( (str, n,i,{length}) => str+(i===length-1 ? length > 1 ? '` and `':'' : i===0 ? '':'`, `') + n,'' )
  const missing_packages_str = missing_packages.join(' ')
  const str = `the following binaries are missing: \`${missing_binaries_str}\`.`
  const install_str = missing_packages_str && packageManager ? "You can install the missing packages by running\n"+packageManager.replace(/%s/g, missing_packages_str): ''
  const err_str = str+`\n`+install_str
  throw new Error(err_str)
}

const test = async () => {
  const assert = require('assert').strict
  const ok = (msg) => console.log("\x1b[32m","✓","\x1b[0m",msg)
  const no = (msg) => console.log("\x1b[31m","✗","\x1b[0m",msg)
  const test = async (name,fn) => {
    try{ 
      await fn(); 
      ok(name) 
    }catch(err){ 
      no(name+'\n    '+err.message) 
    }
  }
  const testThrow = async (name,fn,errorMessage) => {
    try{
      await fn() 
      await test( name, () => assert.strictEqual("",errorMessage))
    }catch(e){
      await test( name, () => assert.strictEqual(e.message,errorMessage))
    }
  }
  await test(
    'can use an array of strings',
    async () => assert.strictEqual(await verify_dependency_list(['gcc']),true)
  )
  await test(
    'can use an array of string[]',
    async () => assert.strictEqual(await verify_dependency_list([['gcc','base-devel']]),true)
  )
  await test(
    'can use an array of {binary:string, package:string }',
    async () => assert.strictEqual(await verify_dependency_list([{binary:'gcc',package:'base-devel'}]),true)
  )
  await testThrow(
    'throws if dependency string not found',
    () => verify_dependency_list(['gccc']),
    "the following binaries are missing: `gccc`.\n"
  )
  await testThrow(
    'throws if dependency string[] not found',
    () => verify_dependency_list([['gccc','base-devel'],['bashh','base-devel']]),
    "the following binaries are missing: `gccc` and `bashh`.\nYou can install the missing packages by running\npacman -S base-devel --needed"
  )
  await testThrow(
    'throws if dependency {binary:string, package:string } not found',
    () => verify_dependency_list([{binary:'gccc',package:'base-devel'}]),
    "the following binaries are missing: `gccc`.\nYou can install the missing packages by running\npacman -S base-devel --needed"
  )
  await testThrow(
    'package install hint can be customized',
    () => verify_dependency_list([{binary:'gccc',package:'base-devel'}],'apt-get install %s'),
    "the following binaries are missing: `gccc`.\nYou can install the missing packages by running\napt-get install base-devel"
  )
  ok('-- All passed!')
}

module.exports = {
	get_paths,
	file_exists,
	verify_dependency,
  verify_dependency_list,
  test
}

if(!module.parent){
  test()
}