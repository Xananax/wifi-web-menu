#!/usr/bin/env node

const { check_dependencies } = require('./get_wifi')

const run = async () => {
  try{
    await check_dependencies()
    require('./web_server')
  }catch(e){
    console.error(e.message)
    exit(1)
  }
}

run()